<?php

namespace Drupal\ad_choices_link\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure ad_choices_link settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The route building service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * Constructs a \Drupal\ad_choices_link\Form\SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The route building service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteBuilderInterface $route_builder) {
    parent::__construct($config_factory);
    $this->routeBuilder = $route_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ad_choices_link_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ad_choices_link.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ad_choices_link.settings');
    $options = ['' => '-- None --'] + menu_ui_get_menus();
    $states = [
      'invisible' => [
        "select[name='ad_choices_link_menu']" => ['value' => ''],
      ],
      'required' => [
        "select[name='ad_choices_link_menu']" => ['!value' => ''],
      ],
    ];
    $form['menu'] = [
      '#type' => 'select',
      '#title' => $this->t('AdChoices Menu Item'),
      '#description' => $this->t("Add the AdChoices menu item to this menu."),
      '#options' => $options,
      '#default_value' => $config->get('menu'),
    ];
    $form['pid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('PID'),
      '#default_value' => $config->get('pid'),
      '#states' => $states,
      '#size' => 5,
      '#maxlength' => 10,
    ];
    $form['ocid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OCID'),
      '#default_value' => $config->get('ocid'),
      '#states' => $states,
      '#size' => 5,
      '#maxlength' => 10,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Make sure a pid and ocid are set if a menu was chosen.
    if (!empty($form_state->getValue('menu'))) {
      if (!is_numeric($form_state->getValue('pid'))) {
        $form_state->setErrorByName('pid', $this->t('The PID must be set and be an integer.'));
      }
      if (!is_numeric($form_state->getValue('ocid'))) {
        $form_state->setErrorByName('ad_choices_link_ocid', $this->t('The OCID must be set and be an integer.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('ad_choices_link.settings')
      ->set('menu', $form_state->getValue('menu'))
      ->set('pid', $form_state->getValue('pid'))
      ->set('ocid', $form_state->getValue('ocid'))
      ->save();
    $this->routeBuilder->rebuild();
    parent::submitForm($form, $form_state);
  }

}
